-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Apr 03, 2019 at 06:31 PM
-- Server version: 10.2.3-MariaDB-log
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel-blog`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'defalut.png',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `slug`, `image`, `created_at`, `updated_at`) VALUES
(3, 'Laravel Framework', 'laravel-framework', '19-03-27_Laravel Framework.jpg', '2019-03-27 08:53:43', '2019-03-27 08:53:43'),
(4, 'PHP', 'php', '19-03-27_PHP.jpg', '2019-03-27 08:53:59', '2019-03-27 08:53:59'),
(5, 'JavaScript', 'javascript', '19-03-27_JavaScript.jpg', '2019-03-27 08:54:17', '2019-03-27 08:54:17'),
(6, 'Angular JS', 'angular-js', '19-03-27_Angular JS.jpg', '2019-03-27 09:00:47', '2019-03-27 09:00:47'),
(7, 'Facebook', 'facebook', '19-03-27_Facebook.jpg', '2019-03-27 11:00:20', '2019-03-27 11:00:20'),
(8, 'Yahoo', 'yahoo', '19-03-27_Yahoo.jpg', '2019-03-27 11:00:31', '2019-03-27 11:00:31'),
(9, 'Digital Marketing', 'digital-marketing', '19-03-28_Digital Marketing.jpg', '2019-03-28 06:52:10', '2019-03-28 06:52:10'),
(10, 'Affilate Marketing', 'affilate-marketing', '19-03-28_Affilate Marketing.JPG', '2019-03-28 06:52:46', '2019-03-28 06:52:46'),
(11, 'PHP OOP', 'php-oop', '19-03-29_PHP OOP.jpg', '2019-03-29 00:46:01', '2019-03-29 00:46:01'),
(12, 'C++ With OOP', 'c-with-oop', '19-04-03_C++ With OOP.JPG', '2019-03-29 23:51:32', '2019-04-03 01:08:55');

-- --------------------------------------------------------

--
-- Table structure for table `category_post`
--

CREATE TABLE `category_post` (
  `id` int(10) UNSIGNED NOT NULL,
  `post_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `category_post`
--

INSERT INTO `category_post` (`id`, `post_id`, `category_id`, `created_at`, `updated_at`) VALUES
(38, 25, 3, '2019-03-28 03:38:36', '2019-03-28 03:38:36'),
(39, 26, 4, '2019-03-28 03:49:12', '2019-03-28 03:49:12'),
(40, 28, 3, '2019-03-28 04:23:01', '2019-03-28 04:23:01'),
(41, 28, 4, '2019-03-28 04:23:01', '2019-03-28 04:23:01'),
(44, 30, 8, '2019-03-28 05:19:45', '2019-03-28 05:19:45'),
(45, 31, 8, '2019-03-28 06:41:13', '2019-03-28 06:41:13'),
(46, 33, 4, '2019-03-28 06:43:45', '2019-03-28 06:43:45'),
(47, 34, 10, '2019-03-28 11:06:01', '2019-03-28 11:06:01'),
(48, 35, 10, '2019-03-28 11:32:18', '2019-03-28 11:32:18'),
(49, 36, 4, '2019-03-28 11:34:38', '2019-03-28 11:34:38'),
(50, 37, 9, '2019-03-28 12:27:30', '2019-03-28 12:27:30'),
(51, 38, 5, '2019-03-28 12:30:43', '2019-03-28 12:30:43'),
(52, 39, 3, '2019-03-28 12:40:55', '2019-03-28 12:40:55'),
(53, 40, 9, '2019-03-29 00:00:04', '2019-03-29 00:00:04'),
(54, 40, 10, '2019-03-29 00:00:04', '2019-03-29 00:00:04'),
(55, 42, 9, '2019-03-29 00:04:48', '2019-03-29 00:04:48'),
(56, 42, 10, '2019-03-29 00:04:48', '2019-03-29 00:04:48'),
(57, 43, 6, '2019-03-29 12:50:30', '2019-03-29 12:50:30'),
(58, 43, 7, '2019-03-29 12:50:30', '2019-03-29 12:50:30'),
(59, 43, 8, '2019-03-29 12:50:30', '2019-03-29 12:50:30'),
(60, 43, 9, '2019-03-29 12:50:30', '2019-03-29 12:50:30'),
(61, 43, 10, '2019-03-29 12:50:30', '2019-03-29 12:50:30'),
(62, 45, 3, '2019-03-30 00:27:38', '2019-03-30 00:27:38'),
(63, 46, 5, '2019-03-30 00:31:32', '2019-03-30 00:31:32'),
(64, 47, 3, '2019-03-30 00:36:03', '2019-03-30 00:36:03'),
(65, 48, 4, '2019-03-30 00:42:21', '2019-03-30 00:42:21'),
(66, 49, 3, '2019-03-30 00:44:12', '2019-03-30 00:44:12'),
(67, 49, 4, '2019-03-30 00:44:12', '2019-03-30 00:44:12'),
(70, 51, 3, '2019-03-30 00:47:43', '2019-03-30 00:47:43'),
(71, 50, 5, '2019-04-02 12:14:10', '2019-04-02 12:14:10'),
(72, 51, 9, '2019-04-02 21:32:41', '2019-04-02 21:32:41'),
(73, 53, 3, '2019-04-03 01:13:26', '2019-04-03 01:13:26'),
(74, 53, 9, '2019-04-03 01:13:26', '2019-04-03 01:13:26'),
(75, 58, 3, '2019-04-03 01:35:06', '2019-04-03 01:35:06'),
(76, 58, 9, '2019-04-03 01:35:06', '2019-04-03 01:35:06'),
(77, 59, 10, '2019-04-03 12:10:18', '2019-04-03 12:10:18');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `post_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `post_id`, `user_id`, `comment`, `created_at`, `updated_at`) VALUES
(1, 45, 2, 'Hello I am Author.', '2019-04-02 00:53:51', '2019-04-02 00:53:51'),
(2, 45, 2, 'Hello I am Author.', '2019-04-02 00:54:34', '2019-04-02 00:54:34'),
(3, 45, 2, 'This is for texting', '2019-04-02 00:58:17', '2019-04-02 00:58:17'),
(7, 47, 2, 'hello', '2019-04-02 04:53:27', '2019-04-02 04:53:27'),
(8, 30, 2, 'HELLO SAJJAD', '2019-04-02 05:06:01', '2019-04-02 05:06:01'),
(9, 48, 2, 'hello', '2019-04-02 05:09:32', '2019-04-02 05:09:32'),
(10, 45, 2, 'hello sorad', '2019-04-02 05:11:11', '2019-04-02 05:11:11'),
(11, 30, 2, 'hello', '2019-04-02 05:12:21', '2019-04-02 05:12:21'),
(12, 33, 1, 'hello monamona', '2019-04-02 12:08:25', '2019-04-02 12:08:25'),
(13, 59, 1, 'i love you tooo monamona', '2019-04-03 12:12:19', '2019-04-03 12:12:19');

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `queue`, `payload`, `attempts`, `reserved_at`, `available_at`, `created_at`) VALUES
(1, 'default', '{\"displayName\":\"App\\\\Notifications\\\\NewAuthorPost\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":11:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:8:\\\"App\\\\User\\\";s:2:\\\"id\\\";i:1;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:31:\\\"App\\\\Notifications\\\\NewAuthorPost\\\":9:{s:4:\\\"post\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:8:\\\"App\\\\Post\\\";s:2:\\\"id\\\";i:51;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:2:\\\"id\\\";s:36:\\\"7db597ec-525a-49d6-b166-cb11a05da165\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1554262363, 1554262363),
(2, 'default', '{\"displayName\":\"App\\\\Notifications\\\\AuthorPostApproved\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":11:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:8:\\\"App\\\\User\\\";s:2:\\\"id\\\";i:2;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:36:\\\"App\\\\Notifications\\\\AuthorPostApproved\\\":9:{s:4:\\\"post\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:8:\\\"App\\\\Post\\\";s:2:\\\"id\\\";i:51;s:9:\\\"relations\\\";a:1:{i:0;s:4:\\\"user\\\";}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:2:\\\"id\\\";s:36:\\\"4bd79dbf-7814-4442-90dd-50bfc7945374\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1554262621, 1554262621),
(3, 'default', '{\"displayName\":\"App\\\\Notifications\\\\NewPostNotify\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":11:{s:11:\\\"notifiables\\\";O:44:\\\"Illuminate\\\\Notifications\\\\AnonymousNotifiable\\\":1:{s:6:\\\"routes\\\";a:1:{s:4:\\\"mail\\\";s:27:\\\"mohammadsajjad585@gmail.com\\\";}}s:12:\\\"notification\\\";O:31:\\\"App\\\\Notifications\\\\NewPostNotify\\\":9:{s:4:\\\"post\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:8:\\\"App\\\\Post\\\";s:2:\\\"id\\\";i:51;s:9:\\\"relations\\\";a:1:{i:0;s:4:\\\"user\\\";}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:2:\\\"id\\\";s:36:\\\"c05f1d8c-beb0-4d6f-a1e4-623ecff91445\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1554262621, 1554262621),
(4, 'default', '{\"displayName\":\"App\\\\Notifications\\\\NewPostNotify\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":11:{s:11:\\\"notifiables\\\";O:44:\\\"Illuminate\\\\Notifications\\\\AnonymousNotifiable\\\":1:{s:6:\\\"routes\\\";a:1:{s:4:\\\"mail\\\";s:23:\\\"rashedctg1719@gmail.com\\\";}}s:12:\\\"notification\\\";O:31:\\\"App\\\\Notifications\\\\NewPostNotify\\\":9:{s:4:\\\"post\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:8:\\\"App\\\\Post\\\";s:2:\\\"id\\\";i:51;s:9:\\\"relations\\\";a:1:{i:0;s:4:\\\"user\\\";}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:2:\\\"id\\\";s:36:\\\"cffbc4ab-0d5a-40ef-b84b-38ada685af50\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1554262621, 1554262621),
(5, 'default', '{\"displayName\":\"App\\\\Notifications\\\\NewPostNotify\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":11:{s:11:\\\"notifiables\\\";O:44:\\\"Illuminate\\\\Notifications\\\\AnonymousNotifiable\\\":1:{s:6:\\\"routes\\\";a:1:{s:4:\\\"mail\\\";s:26:\\\"mohiuddinctg1719@gmail.com\\\";}}s:12:\\\"notification\\\";O:31:\\\"App\\\\Notifications\\\\NewPostNotify\\\":9:{s:4:\\\"post\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:8:\\\"App\\\\Post\\\";s:2:\\\"id\\\";i:51;s:9:\\\"relations\\\";a:1:{i:0;s:4:\\\"user\\\";}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:2:\\\"id\\\";s:36:\\\"aedc2652-b646-4985-b78d-900aff6a2f90\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1554262622, 1554262622),
(6, 'default', '{\"displayName\":\"App\\\\Notifications\\\\NewPostNotify\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":11:{s:11:\\\"notifiables\\\";O:44:\\\"Illuminate\\\\Notifications\\\\AnonymousNotifiable\\\":1:{s:6:\\\"routes\\\";a:1:{s:4:\\\"mail\\\";s:19:\\\"irmdmitul@gmail.com\\\";}}s:12:\\\"notification\\\";O:31:\\\"App\\\\Notifications\\\\NewPostNotify\\\":9:{s:4:\\\"post\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:8:\\\"App\\\\Post\\\";s:2:\\\"id\\\";i:51;s:9:\\\"relations\\\";a:1:{i:0;s:4:\\\"user\\\";}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:2:\\\"id\\\";s:36:\\\"6f9c0ee4-4387-43b5-ae1a-929cd739cbc1\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1554262622, 1554262622),
(7, 'default', '{\"displayName\":\"App\\\\Notifications\\\\NewPostNotify\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":11:{s:11:\\\"notifiables\\\";O:44:\\\"Illuminate\\\\Notifications\\\\AnonymousNotifiable\\\":1:{s:6:\\\"routes\\\";a:1:{s:4:\\\"mail\\\";s:24:\\\"israt.rani3023@gmail.com\\\";}}s:12:\\\"notification\\\";O:31:\\\"App\\\\Notifications\\\\NewPostNotify\\\":9:{s:4:\\\"post\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:8:\\\"App\\\\Post\\\";s:2:\\\"id\\\";i:51;s:9:\\\"relations\\\";a:1:{i:0;s:4:\\\"user\\\";}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:2:\\\"id\\\";s:36:\\\"1d76c032-e30a-4736-b195-1fae2fa519ed\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1554262622, 1554262622),
(8, 'default', '{\"displayName\":\"App\\\\Notifications\\\\NewAuthorPost\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":11:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:8:\\\"App\\\\User\\\";s:2:\\\"id\\\";i:1;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:31:\\\"App\\\\Notifications\\\\NewAuthorPost\\\":9:{s:4:\\\"post\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:8:\\\"App\\\\Post\\\";s:2:\\\"id\\\";i:59;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:2:\\\"id\\\";s:36:\\\"9c60cf75-d5b7-4518-8563-a56af1c2c5d0\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1554315022, 1554315022),
(9, 'default', '{\"displayName\":\"App\\\\Notifications\\\\AuthorPostApproved\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":11:{s:11:\\\"notifiables\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:8:\\\"App\\\\User\\\";s:2:\\\"id\\\";i:4;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:12:\\\"notification\\\";O:36:\\\"App\\\\Notifications\\\\AuthorPostApproved\\\":9:{s:4:\\\"post\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:8:\\\"App\\\\Post\\\";s:2:\\\"id\\\";i:59;s:9:\\\"relations\\\";a:1:{i:0;s:4:\\\"user\\\";}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:2:\\\"id\\\";s:36:\\\"c523f08b-e951-46da-b05d-59c1b5f48f5d\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1554315066, 1554315066),
(10, 'default', '{\"displayName\":\"App\\\\Notifications\\\\NewPostNotify\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":11:{s:11:\\\"notifiables\\\";O:44:\\\"Illuminate\\\\Notifications\\\\AnonymousNotifiable\\\":1:{s:6:\\\"routes\\\";a:1:{s:4:\\\"mail\\\";s:27:\\\"mohammadsajjad585@gmail.com\\\";}}s:12:\\\"notification\\\";O:31:\\\"App\\\\Notifications\\\\NewPostNotify\\\":9:{s:4:\\\"post\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:8:\\\"App\\\\Post\\\";s:2:\\\"id\\\";i:59;s:9:\\\"relations\\\";a:1:{i:0;s:4:\\\"user\\\";}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:2:\\\"id\\\";s:36:\\\"3c6ba367-1361-443b-83f6-0eb204723369\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1554315066, 1554315066),
(11, 'default', '{\"displayName\":\"App\\\\Notifications\\\\NewPostNotify\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":11:{s:11:\\\"notifiables\\\";O:44:\\\"Illuminate\\\\Notifications\\\\AnonymousNotifiable\\\":1:{s:6:\\\"routes\\\";a:1:{s:4:\\\"mail\\\";s:23:\\\"rashedctg1719@gmail.com\\\";}}s:12:\\\"notification\\\";O:31:\\\"App\\\\Notifications\\\\NewPostNotify\\\":9:{s:4:\\\"post\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:8:\\\"App\\\\Post\\\";s:2:\\\"id\\\";i:59;s:9:\\\"relations\\\";a:1:{i:0;s:4:\\\"user\\\";}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:2:\\\"id\\\";s:36:\\\"7679d232-6f89-4136-af3e-a09a1780502a\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1554315067, 1554315067),
(12, 'default', '{\"displayName\":\"App\\\\Notifications\\\\NewPostNotify\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":11:{s:11:\\\"notifiables\\\";O:44:\\\"Illuminate\\\\Notifications\\\\AnonymousNotifiable\\\":1:{s:6:\\\"routes\\\";a:1:{s:4:\\\"mail\\\";s:26:\\\"mohiuddinctg1719@gmail.com\\\";}}s:12:\\\"notification\\\";O:31:\\\"App\\\\Notifications\\\\NewPostNotify\\\":9:{s:4:\\\"post\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:8:\\\"App\\\\Post\\\";s:2:\\\"id\\\";i:59;s:9:\\\"relations\\\";a:1:{i:0;s:4:\\\"user\\\";}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:2:\\\"id\\\";s:36:\\\"d7bb03cd-0530-41e2-9b31-8209ba7594d1\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1554315067, 1554315067),
(13, 'default', '{\"displayName\":\"App\\\\Notifications\\\\NewPostNotify\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":11:{s:11:\\\"notifiables\\\";O:44:\\\"Illuminate\\\\Notifications\\\\AnonymousNotifiable\\\":1:{s:6:\\\"routes\\\";a:1:{s:4:\\\"mail\\\";s:19:\\\"irmdmitul@gmail.com\\\";}}s:12:\\\"notification\\\";O:31:\\\"App\\\\Notifications\\\\NewPostNotify\\\":9:{s:4:\\\"post\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:8:\\\"App\\\\Post\\\";s:2:\\\"id\\\";i:59;s:9:\\\"relations\\\";a:1:{i:0;s:4:\\\"user\\\";}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:2:\\\"id\\\";s:36:\\\"65fed5c7-f570-4165-9a05-48a4d59ccae1\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1554315067, 1554315067),
(14, 'default', '{\"displayName\":\"App\\\\Notifications\\\\NewPostNotify\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\",\"command\":\"O:48:\\\"Illuminate\\\\Notifications\\\\SendQueuedNotifications\\\":11:{s:11:\\\"notifiables\\\";O:44:\\\"Illuminate\\\\Notifications\\\\AnonymousNotifiable\\\":1:{s:6:\\\"routes\\\";a:1:{s:4:\\\"mail\\\";s:24:\\\"israt.rani3023@gmail.com\\\";}}s:12:\\\"notification\\\";O:31:\\\"App\\\\Notifications\\\\NewPostNotify\\\":9:{s:4:\\\"post\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:8:\\\"App\\\\Post\\\";s:2:\\\"id\\\";i:59;s:9:\\\"relations\\\";a:1:{i:0;s:4:\\\"user\\\";}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:2:\\\"id\\\";s:36:\\\"6088a33a-e138-4936-aa12-128882a841f0\\\";s:6:\\\"locale\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}s:8:\\\"channels\\\";a:1:{i:0;s:4:\\\"mail\\\";}s:5:\\\"tries\\\";N;s:7:\\\"timeout\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";N;s:7:\\\"chained\\\";a:0:{}}\"}}', 0, NULL, 1554315067, 1554315067);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_03_25_042324_create_roles_table', 1),
(4, '2019_03_25_123917_create_tags_table', 1),
(5, '2019_03_26_063041_create_categories_table', 1),
(6, '2019_03_27_034504_create_posts_table', 1),
(7, '2019_03_27_034713_create_category_post_table', 1),
(8, '2019_03_27_034951_create_post_tag_table', 1),
(9, '2019_03_28_115202_create_subscribers_table', 2),
(10, '2019_03_29_054939_create_jobs_table', 3),
(11, '2019_03_30_154548_create_post_user_table', 4),
(12, '2019_04_02_061834_create_comments_table', 5);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'default.png',
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `view_count` int(11) NOT NULL DEFAULT 0,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `is_approved` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `user_id`, `title`, `slug`, `image`, `body`, `view_count`, `status`, `is_approved`, `created_at`, `updated_at`) VALUES
(25, 1, 'Laravel', 'laravel', '19-03-28_laravel.jpg', '<p>The Laravel framework has a few system requirements. All of these requirements are satisfied by the <a href=\"https://laravel.com/docs/5.8/homestead\">Laravel Homestead</a> virtual machine, so it\'s highly recommended that you use Homestead as your local Laravel development environment.</p>', 2, 1, 1, '2019-03-28 03:38:36', '2019-04-03 06:58:31'),
(26, 1, 'PHP', 'php', '19-03-28_php.jpg', '<p>this is php</p>', 1, 0, 1, '2019-03-28 03:49:12', '2019-04-02 11:43:51'),
(28, 2, 'Author post is here', 'author-post-is-here', '19-03-28_author-post-is-here.jpg', '<p>this is body</p>', 3, 1, 1, '2019-03-28 04:23:01', '2019-04-02 22:10:22'),
(30, 2, 'Python', 'python', '19-03-28_python.jpg', '<p>This is Python body</p>', 2, 1, 1, '2019-03-28 05:19:45', '2019-04-02 21:13:12'),
(31, 2, 'The Power Of LOVE', 'the-power-of-love', '19-03-28_the-power-of-love.jpg', '<p>Valeri Taylor whose work on disability issues in bangladesh has transformed countless lives for the better turns 75 today.</p>\r\n<h4 class=\"title\"><span style=\"text-decoration: underline;\"><a href=\"../../\"><strong>How Did Van Gogh\'s Turbulent Mind Depict One of the Most Complex Concepts in Physics?</strong></a></span></h4>', 2, 1, 1, '2019-03-28 06:41:10', '2019-04-02 21:12:57'),
(33, 1, 'PHP Language', 'php-language', '19-03-28_php.jpg', '<p>this is php</p>', 3, 1, 1, '2019-03-28 06:43:45', '2019-04-03 07:12:44'),
(34, 2, 'New Arrival laptop brand', 'new-arrival-laptop-brand', '19-03-28_new-arrival-laptop-brand.jpg', '<p>this is for email sending</p>', 0, 1, 1, '2019-03-28 11:06:01', '2019-03-28 12:32:12'),
(35, 2, 'eMAIL TESTING', 'email-testing', '19-03-28_email-testing.jpg', '<p>this is email testing</p>', 1, 1, 1, '2019-03-28 11:32:17', '2019-04-02 11:54:26'),
(36, 2, 'New Arrival laptop brand 2', 'new-arrival-laptop-brand-2', '19-03-28_new-arrival-laptop-brand-2.jpg', '<p>dfsdljsfljsk</p>', 1, 1, 1, '2019-03-28 11:34:37', '2019-04-02 11:41:15'),
(37, 1, 'admin is here', 'admin-is-here', '19-03-28_admin-is-here.jpg', '<p>this is for subscribers..</p>', 0, 1, 1, '2019-03-28 12:27:30', '2019-03-28 12:27:30'),
(38, 1, 'This Is for Laravel', 'this-is-for-laravel', '19-03-28_this-is-for-laravel.jpg', '<p>i love you monamona</p>', 0, 1, 1, '2019-03-28 12:30:41', '2019-03-28 12:30:41'),
(39, 2, 'This is for testing', 'this-is-for-testing', '19-03-28_this-is-for-testing.jpg', '<p>this sdfslfsdslfsfslfsdkjdlgdjgk jkjfsdjfl jafsf jasfsdkjf slfds fsdfjsdsl ajasdfksj f g jfowueo jjgs;jgjr traofst;&nbsp; toj t jotuuio jfala wooiteut mfsdl jgjsj gsdlt;sl ufdog</p>', 2, 1, 1, '2019-03-28 12:40:55', '2019-04-02 11:27:40'),
(40, 1, 'Queue Worker', 'queue-worker', '19-03-29_queue-worker.jpg', '<p>This is Queue for jobs table working</p>', 1, 1, 1, '2019-03-29 00:00:04', '2019-04-02 11:53:26'),
(42, 1, 'Queue Worker again', 'queue-worker-again', '19-03-29_queue-worker-again.jpg', '<p>This is Queue for jobs table working</p>', 2, 1, 1, '2019-03-29 00:04:48', '2019-04-02 12:00:08'),
(43, 1, 'Selecting best blogging platform', 'selecting-best-blogging-platform', '19-03-29_selecting-best-blogging-platform.png', '<p>The biggest mistake beginners make when building a blog is choosing the wrong blogging platform. Thankfully you&rsquo;re here, so you won&rsquo;t be making that mistake.</p>\r\n<p>For 95% of users, it makes more sense to&nbsp;<strong>use WordPress.org</strong>&nbsp;also known as self-hosted WordPress. Why? Because it is free to use, you can install plugins, customize your site design, and most importantly make money from your site without any restrictions.</p>\r\n<p>WordPress by far is one of the biggest global blogging platform. It offers thousands of plugins and add-ons and flexibility and liberty to change design and layout of your new blog.</p>\r\n<p>As per a recent statistic report 82 million users are active on WordPress.</p>\r\n<p>Now you have probably heard that&nbsp;WordPress is free.</p>\r\n<p>You might be wondering why is it free? What&rsquo;s the catch?</p>\r\n<p>There&rsquo;s no catch. It&rsquo;s free because you have to do the setup and host it yourself.</p>\r\n<p>In other words, you need a&nbsp;<strong>domain name and web hosting</strong>.</p>\r\n<p>A domain name is what people type to get to your website. It&rsquo;s your website&rsquo;s address on the internet. Think google.com or bloggerterminal.com</p>', 4, 1, 1, '2019-03-29 12:50:29', '2019-04-02 12:03:02'),
(45, 1, 'Laravel Framework', 'laravel-framework', '19-03-30_laravel-framework.jpg', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim nostrud exercitation ullamco laboris nisi ut aliquip</p>', 4, 1, 1, '2019-03-30 00:27:38', '2019-04-02 12:04:10'),
(46, 1, 'Introduction of JavaScript', 'introduction-of-javascript', '19-03-30_introduction-of-javascript.jpg', '<h4>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h4>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim nostrud exercitation ullamco laboris nisi ut aliquip</p>', 1, 1, 1, '2019-03-30 00:31:32', '2019-04-01 23:40:54'),
(47, 1, 'this is check', 'this-is-check', '19-03-30_this-is-check.jpg', '<h4>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h4>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim nostrud exercitation ullamco laboris nisi ut aliquip</p>', 4, 1, 1, '2019-03-30 00:36:03', '2019-04-02 11:16:17'),
(48, 1, 'this is cruds', 'this-is-cruds', '19-03-30_this-is-cruds.jpg', '<h4>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h4>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim nostrud exercitation ullamco laboris nisi ut aliquip</p>', 10, 1, 1, '2019-03-30 00:42:21', '2019-04-03 11:57:57'),
(50, 1, 'Introduction of JavaScripts', 'introduction-of-javascripts', '19-03-30_introduction-of-javascript.jpg', '<h4>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h4>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim nostrud exercitation ullamco laboris nisi ut aliquip</p>', 2, 1, 1, '2019-04-02 12:14:10', '2019-04-03 06:31:37'),
(51, 2, 'ISPS Code for Chittagong port', 'isps-code-for-chittagong-port', '19-04-03_isps-code-for-chittagong-port.JPG', '<p>this is for approved test</p>', 0, 0, 1, '2019-04-02 21:32:40', '2019-04-02 21:37:00'),
(53, 1, 'ISPS Code for Chittagong ports', 'isps-code-for-chittagong-ports', '19-04-03_isps-code-for-chittagong-port.JPG', '<p>this is for approved test</p>', 0, 1, 1, '2019-04-03 01:13:26', '2019-04-03 01:13:26'),
(58, 1, 'ISPS Code for Chittagong ports', NULL, '19-04-03_isps-code-for-chittagong-port.JPG', '<p>this is for approved test</p>', 0, 0, 1, '2019-04-03 01:35:05', '2019-04-03 01:35:05'),
(59, 4, 'I Love You', 'i-love-you', '19-04-03_i-love-you.jpg', '<p>I love you so much monamona</p>', 1, 1, 1, '2019-04-03 12:10:18', '2019-04-03 12:11:19');

-- --------------------------------------------------------

--
-- Table structure for table `post_tag`
--

CREATE TABLE `post_tag` (
  `id` int(10) UNSIGNED NOT NULL,
  `post_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `post_tag`
--

INSERT INTO `post_tag` (`id`, `post_id`, `tag_id`, `created_at`, `updated_at`) VALUES
(32, 25, 3, '2019-03-28 03:38:36', '2019-03-28 03:38:36'),
(33, 26, 4, '2019-03-28 03:49:13', '2019-03-28 03:49:13'),
(34, 28, 3, '2019-03-28 04:23:01', '2019-03-28 04:23:01'),
(38, 30, 7, '2019-03-28 05:19:45', '2019-03-28 05:19:45'),
(39, 31, 7, '2019-03-28 06:41:14', '2019-03-28 06:41:14'),
(40, 33, 4, '2019-03-28 06:43:46', '2019-03-28 06:43:46'),
(41, 34, 10, '2019-03-28 11:06:01', '2019-03-28 11:06:01'),
(42, 35, 10, '2019-03-28 11:32:18', '2019-03-28 11:32:18'),
(43, 36, 9, '2019-03-28 11:34:38', '2019-03-28 11:34:38'),
(44, 37, 10, '2019-03-28 12:27:30', '2019-03-28 12:27:30'),
(45, 38, 10, '2019-03-28 12:30:43', '2019-03-28 12:30:43'),
(46, 39, 9, '2019-03-28 12:40:55', '2019-03-28 12:40:55'),
(47, 39, 10, '2019-03-28 12:40:55', '2019-03-28 12:40:55'),
(48, 40, 9, '2019-03-29 00:00:04', '2019-03-29 00:00:04'),
(49, 40, 10, '2019-03-29 00:00:04', '2019-03-29 00:00:04'),
(50, 42, 9, '2019-03-29 00:04:48', '2019-03-29 00:04:48'),
(51, 42, 10, '2019-03-29 00:04:48', '2019-03-29 00:04:48'),
(52, 43, 4, '2019-03-29 12:50:30', '2019-03-29 12:50:30'),
(53, 43, 9, '2019-03-29 12:50:30', '2019-03-29 12:50:30'),
(54, 43, 10, '2019-03-29 12:50:30', '2019-03-29 12:50:30'),
(55, 45, 3, '2019-03-30 00:27:38', '2019-03-30 00:27:38'),
(56, 46, 5, '2019-03-30 00:31:32', '2019-03-30 00:31:32'),
(57, 47, 3, '2019-03-30 00:36:04', '2019-03-30 00:36:04'),
(58, 47, 4, '2019-03-30 00:36:04', '2019-03-30 00:36:04'),
(59, 48, 9, '2019-03-30 00:42:22', '2019-03-30 00:42:22'),
(60, 49, 6, '2019-03-30 00:44:12', '2019-03-30 00:44:12'),
(61, 49, 7, '2019-03-30 00:44:12', '2019-03-30 00:44:12'),
(62, 49, 8, '2019-03-30 00:44:12', '2019-03-30 00:44:12'),
(66, 51, 9, '2019-03-30 00:47:43', '2019-03-30 00:47:43'),
(67, 50, 5, '2019-04-02 12:14:11', '2019-04-02 12:14:11'),
(68, 51, 10, '2019-04-02 21:32:41', '2019-04-02 21:32:41'),
(69, 53, 9, '2019-04-03 01:13:27', '2019-04-03 01:13:27'),
(70, 53, 10, '2019-04-03 01:13:27', '2019-04-03 01:13:27'),
(71, 58, 9, '2019-04-03 01:35:06', '2019-04-03 01:35:06'),
(72, 58, 10, '2019-04-03 01:35:06', '2019-04-03 01:35:06'),
(73, 59, 10, '2019-04-03 12:10:18', '2019-04-03 12:10:18');

-- --------------------------------------------------------

--
-- Table structure for table `post_user`
--

CREATE TABLE `post_user` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `post_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `post_user`
--

INSERT INTO `post_user` (`id`, `post_id`, `user_id`, `created_at`, `updated_at`) VALUES
(8, 46, 1, '2019-03-30 23:03:47', '2019-03-30 23:03:47'),
(14, 47, 1, '2019-03-31 01:14:49', '2019-03-31 01:14:49'),
(15, 48, 1, '2019-04-01 22:57:25', '2019-04-01 22:57:25'),
(16, 39, 1, '2019-04-02 12:14:26', '2019-04-02 12:14:26'),
(17, 48, 2, '2019-04-02 12:26:50', '2019-04-02 12:26:50'),
(18, 53, 4, '2019-04-03 12:16:42', '2019-04-03 12:16:42');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin', NULL, NULL),
(2, 'Author', 'author', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `subscribers`
--

CREATE TABLE `subscribers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subscribers`
--

INSERT INTO `subscribers` (`id`, `email`, `created_at`, `updated_at`) VALUES
(1, 'mohammadsajjad585@gmail.com', '2019-03-28 08:35:51', '2019-03-28 08:35:51'),
(2, 'rashedctg1719@gmail.com', '2019-03-28 23:22:58', '2019-03-28 23:22:58'),
(3, 'mohiuddinctg1719@gmail.com', '2019-03-28 23:23:44', '2019-03-28 23:23:44'),
(4, 'irmdmitul@gmail.com', '2019-03-29 07:37:21', '2019-03-29 07:37:21'),
(5, 'israt.rani3023@gmail.com', '2019-03-29 12:46:59', '2019-03-29 12:46:59');

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(3, 'Laravel', 'laravel', '2019-03-27 08:52:49', '2019-03-27 08:52:49'),
(4, 'PHP', 'php', '2019-03-27 08:52:59', '2019-03-27 08:52:59'),
(5, 'JavaScript', 'javascript', '2019-03-27 08:53:09', '2019-03-27 08:53:09'),
(6, 'Facebook', 'facebook', '2019-03-27 10:59:50', '2019-03-27 10:59:50'),
(7, 'Yahoo', 'yahoo', '2019-03-27 10:59:58', '2019-03-27 10:59:58'),
(8, 'Software', 'software', '2019-03-28 06:50:53', '2019-03-28 06:50:53'),
(9, 'SEO', 'seo', '2019-03-28 06:51:20', '2019-03-28 06:51:20'),
(10, 'Back link', 'back-link', '2019-03-28 06:51:33', '2019-03-28 06:51:33'),
(11, 'Java', 'java', '2019-03-29 23:50:04', '2019-03-29 23:50:04'),
(12, 'C, C++', 'c-c', '2019-03-29 23:50:21', '2019-03-29 23:50:21'),
(13, 'Ruby', 'ruby', '2019-03-29 23:50:32', '2019-03-29 23:50:32');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '2',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'default.png',
  `about` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `username`, `email`, `image`, `about`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, '1', 'Mr. Admin', 'admin', 'admin@gmail.com', '19-03-31_Mr. Admin.png', 'I am Admin of this project.', NULL, '$2y$10$OtckM1Tgfj0DzZtT8vpQ/e3IsHaRETyOBFZEEprSlkdhm9xlUK7uK', NULL, '2019-03-25 18:00:00', '2019-03-31 00:44:36'),
(2, '2', 'Ms. Author', 'author', 'author@blog.com', '19-03-31_Ms. Author.jpg', 'I Am Author', NULL, '$2y$10$SnqX0VZVDpefwdXe4aS1BuNOYcfwbuvWfAIhpvHxUlLbpFJd2snpa', NULL, '2019-04-02 18:00:00', '2019-03-31 01:35:23'),
(3, '2', 'Mohammad Sajjad Hossain', 'author2', 'author2@blog.com', 'default.png', NULL, NULL, '$2y$10$Nt1AiBaRtQLaycD6PJ4zPeqmf04MD5m5zTfvENEVCC5a/JV0s1Lem', NULL, '2019-04-03 05:58:11', '2019-04-03 05:58:11'),
(4, '2', 'Rani', 'rani', 'rani@blog.com', '19-04-03_Rani.jpg', NULL, NULL, '$2y$10$pM0Trmq/XJR7L/q3WFvwoOQPKLrh0sVfScLJrFtF7NalJ/CdoBqBy', NULL, '2019-04-03 06:02:43', '2019-04-03 12:08:23'),
(5, '2', 'Monamona', 'monamona', 'monamona@blog.com', '19-04-03_Monamona.jpg', NULL, NULL, '$2y$10$lUDpjQ4r5btNQzn1U4PlgOuAEeYQNkuCbVdW/E2jnfyOXrMrwNvYi', NULL, '2019-04-03 12:18:52', '2019-04-03 12:21:26');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category_post`
--
ALTER TABLE `category_post`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comments_post_id_foreign` (`post_id`),
  ADD KEY `comments_user_id_foreign` (`user_id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_index` (`queue`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `posts_slug_unique` (`slug`),
  ADD KEY `posts_user_id_foreign` (`user_id`);

--
-- Indexes for table `post_tag`
--
ALTER TABLE `post_tag`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post_user`
--
ALTER TABLE `post_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_user_post_id_foreign` (`post_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscribers`
--
ALTER TABLE `subscribers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_username_unique` (`username`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `category_post`
--
ALTER TABLE `category_post`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `post_tag`
--
ALTER TABLE `post_tag`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;

--
-- AUTO_INCREMENT for table `post_user`
--
ALTER TABLE `post_user`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `subscribers`
--
ALTER TABLE `subscribers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `comments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `post_user`
--
ALTER TABLE `post_user`
  ADD CONSTRAINT `post_user_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
