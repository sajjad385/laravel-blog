
<div class="form-group form-float">
    <div class="form-line">
        <input type="file" name="image">
    </div>
</div>

<div class="form-group  form-float">
    <div class="form-line">
    {{ Form::text('title', null, [
        'class' => 'form-control',
        'id' => 'title',
    ]) }}

    {{ Form::label('Category Name',[
        'class' => 'form-label',
    ]) }}
    </div>
</div>


<div class="form-group form-float">
    <div class="form-line">
        {{ Form::file('image', null, [
            'id' => 'image',
        ]) }}
    </div>
</div>


{{ Form::button('Submit', [
    'type' => 'submit',
    'class' => 'btn btn-primary m-t-15 waves-effect',
]) }}
