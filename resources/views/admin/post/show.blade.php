
@extends('backend.layouts.master')
@section('title','Post')


@push('css')
<!-- Bootstrap Select Css -->
<link href="{{asset('ui/backend/plugins/bootstrap-select/css/bootstrap-select.css')}}" rel="stylesheet"/>

@endpush

@section('content')
    <section class="content">
        <div class="container-fluid">
            <a href="{{route('admin.post.index')}}" class="btn btn-danger waves-effect">Back</a>


            @if($post->is_approved == false)
                <button type="button" class="btn btn-success waves-effect pull-right" onclick="approvePost({{$post->id}})">
                    <i class="material-icons">done</i>
                    <span>Approve</span>
                </button>
                <form action="{{route('admin.post.approve',$post->id)}}" id="approval-form" style="display: none" method="POST">

                    @csrf
                    @method('PUT')
                </form>
            @else
                <button type="button" class="btn btn-success waves-effect pull-right" disabled="disabled">
                    <i class="material-icons">done</i>
                    <span>Approved</span>
                </button>
            @endif
            <br><br>


            <br><br>
            <div class="row clearfix">
                <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                {{$post->title}}
                                <small>Posted by
                                    <strong>
                                        <a href="">{{$post->user->name}}</a>
                                    </strong> on {{$post->created_at->toFormattedDateString()}}
                                </small>
                            </h2>
                        </div>
                        <div class="body">
                            {!! $post->body!!}

                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header bg-red">
                            <h2>
                                CATEGORIES
                            </h2>
                        </div>
                        <div class="body">

                            @foreach($post->categories as $category)
                                <span class="label bg-cyan" style="font-size: 13px">{{$category->name}}</span>
                            @endforeach

                        </div>
                    </div>
                    <div class="card">
                        <div class="header bg-blue">
                            <h2>
                                TAGS
                            </h2>
                        </div>
                        <div class="body">

                            @foreach($post->tags as $tag)
                                <span class="label bg-green" style="font-size: 13px;">{{$tag->name}}
                                </span>
                            @endforeach

                        </div>
                    </div>
                <div class="card">
                        <div class="header bg-blue">
                            <h2>
                                FEATURED IMAGE
                            </h2>
                        </div>
                        <div class="body">
                            <img src="{{asset('uploads/post/'.$post->image)}}" class="img-responsive thumbnail" alt="">

                        </div>
                    </div>
                </div>
            </div>


        </div>
    </section>
@endsection

@push('js')

<!-- Select Plugin Js -->
<script src="{{asset('ui/backend/plugins/bootstrap-select/js/bootstrap-select.js')}}"></script>


<!-- TinyMCE -->
<script src="{{asset('ui/backend/plugins/tinymce/tinymce.jquery.js')}}"></script>
<script src="{{asset('ui/backend/plugins/tinymce/tinymce.js')}}"></script>

<script src="https://unpkg.com/sweetalert2@7.19.1/dist/sweetalert2.all.js"></script>

<script type="text/javascript">

    function approvePost(id) {
        Swal.fire({
            title: 'Are you sure?',
            text: 'You  want to approve this  post!',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, Approve it!',
            cancelButtonText: 'No, keep it'
        }).then((result) => {
            if (result.value) {
            Swal.fire(

                event.preventDefault(),
                document.getElementById('approval-form').submit()
            )
            // For more information about handling dismissals please visit
            // https://sweetalert2.github.io/#handling-dismissals
        } else if (result.dismiss === Swal.DismissReason.cancel) {
            Swal.fire(
                'Cancelled',
                'The Post Remain Pending  :)',
                'info'
            )
        }
    });
    }
</script>
<script type="text/javascript">

    //TinyMCE
    tinymce.init({
        selector: "textarea",
        theme: "modern",
        height: 300,
        plugins: [
            'advlist autolink lists link image charmap print preview hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools'
        ],
        toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
        toolbar2: 'print preview media | forecolor backcolor emoticons',
        image_advtab: true
    });
    tinymce.suffix = ".min";
    tinyMCE.baseURL = '{{asset('ui/backend/plugins/tinymce')}}';

</script>
@endpush



