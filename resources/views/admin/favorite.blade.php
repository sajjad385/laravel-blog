@extends('backend.layouts.master')
@section('title','Favorite Post')


@push('css')
<!-- JQuery DataTable Css -->
<link href="{{asset('ui/backend/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}" rel="stylesheet">


@endpush

@section('content')
    <section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2>
                <a class="btn btn-primary waves-effect" href="{{route('admin.post.create')}}">

                    <i class="material-icons">add</i>
                    <span>ADD NEW POST</span>
                </a>
            </h2>
        </div>

        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            ALL FAVORITE POSTS
                            <span class="badge bg-blue">{{$posts->count()}}</span>
                        </h2>
                        
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Title</th>
                                    <th>Author</th>
                                    <th><i class="material-icons">favorite</i></th>
                                    {{--<th><i class="material-icons">comment</i></th>--}}
                                    <th><i class="material-icons">visibility</i></th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>ID</th>
                                    <th>Title</th>
                                    <th>Author</th>
                                    <th><i class="material-icons">favorite</i></th>
                                    {{--<th><i class="material-icons">comment</i></th>--}}
                                    <th><i class="material-icons">visibility</i></th>
                                    <th>Action</th>
                                </tr>
                                </tfoot>
                                <tbody>
                                @foreach($posts as $key=>$post)

                                    <tr>
                                        <td>{{$key +1}}</td>
                                        <td>{{str_limit($post->title,'10')}}</td>
                                        <td>{{$post->user->name}}</td>
                                        <td>{{$post->favorite_to_users->count()}}</td>
                                        <td>{{$post->view_count}}</td>


                                        <td>

                                            <a href="{{route('admin.post.show',$post->id)}}"
                                               class="btn btn-info waves-effect">
                                                <i class="material-icons">visibility</i>
                                            </a>
                                            <button  type="button" class="btn btn-danger waves-effect" onclick="removePost({{$post->id}})">
                                                <i class="material-icons">delete</i>
                                            </button>
                                            <form id="remove-form-{{$post->id}}" action="{{route('post.favorite',$post->id)}}" method="post" style="display: none">

                                                @csrf

                                            </form>
                                        </td>


                                    </tr>
                                  @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->
    </div>
    </section>
@endsection

@push('js') <!-- Jquery Core Js -->
<script src="{{asset('ui/backend/plugins/jquery/jquery.min.js')}}"></script>

<!-- Bootstrap Core Js -->
<script src="{{asset('ui/backend/plugins/bootstrap/js/bootstrap.js')}}"></script>

<!-- Select Plugin Js -->
<script src="{{asset('ui/backend/plugins/bootstrap-select/js/bootstrap-select.js')}}"></script>

<!-- Slimscroll Plugin Js -->
<script src="{{asset('ui/backend/plugins/jquery-slimscroll/jquery.slimscroll.js')}}"></script>

<!-- Waves Effect Plugin Js -->
<script src="{{asset('ui/backend/plugins/node-waves/waves.js')}}"></script>

<!-- Jquery DataTable Plugin Js -->
<script src="{{asset('ui/backend/plugins/jquery-datatable/jquery.dataTables.js')}}"></script>
<script src="{{asset('ui/backend/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')}}"></script>
<script src="{{asset('ui/backend/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('ui/backend/plugins/jquery-datatable/extensions/export/buttons.flash.min.js')}}"></script>
<script src="{{asset('ui/backend/plugins/jquery-datatable/extensions/export/jszip.min.js')}}"></script>
<script src="{{asset('ui/backend/plugins/jquery-datatable/extensions/export/pdfmake.min.js')}}"></script>
<script src="{{asset('ui/backend/plugins/jquery-datatable/extensions/export/vfs_fonts.js')}}"></script>
<script src="{{asset('ui/backend/plugins/jquery-datatable/extensions/export/buttons.html5.min.js')}}"></script>
<script src="{{asset('ui/backend/plugins/jquery-datatable/extensions/export/buttons.print.min.js')}}"></script>

<!-- Custom Js -->
<script src="{{asset('ui/backend/js/admin.js')}}"></script>
<script src="{{asset('ui/backend/js/pages/tables/jquery-datatable.js')}}"></script>

<!-- Demo Js -->
<script src="{{asset('ui/backend/js/demo.js')}}"></script>


<script src="https://unpkg.com/sweetalert2@7.19.1/dist/sweetalert2.all.js"></script>


<script type="text/javascript">

    function removePost(id) {
        Swal.fire({
            title: 'Are you sure?',
            text: 'You will not be able to recover this Tag!',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, remove it!',
            cancelButtonText: 'No, keep it'
        }).then((result) => {
            if (result.value) {
            Swal.fire(

                event.preventDefault(),
                document.getElementById('remove-form-'+id).submit()
            )
            // For more information about handling dismissals please visit
            // https://sweetalert2.github.io/#handling-dismissals
        } else if (result.dismiss === Swal.DismissReason.cancel) {
            Swal.fire(
                'Cancelled',
                'Your Data is safe :)',
                'error'
            )
        }
    });
    }






</script>
@endpush
