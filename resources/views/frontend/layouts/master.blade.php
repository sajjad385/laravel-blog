<!DOCTYPE HTML>
<html lang="en">
<head>
    <title>@yield('title')-{{ config('app.name', 'Laravel') }}</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8">


    <!-- Font -->

    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500" rel="stylesheet">


    <!-- Stylesheets -->

    <link href="{{asset('ui/frontend/css/bootstrap.css')}}" rel="stylesheet">

    <link href="{{asset('ui/frontend/css/swiper.css')}}" rel="stylesheet">
    
    <link href="{{asset('ui/frontend/css/ionicons.css')}}" rel="stylesheet">
    <link href="{{asset('ui/frontend/css/home/styles.css')}}" rel="stylesheet">
    <link href="{{asset('ui/frontend/css/main.css')}}" rel="stylesheet">

    <link rel="stylesheet" href="http://cdn.bootcss.com/toastr.js/latest/css/toastr.min.css">
    
    @stack('css')
    <style>
        header .src-area .src-input {
            position: absolute;
            top: 17px;
            bottom: 0;
            left: 0;
        
        }

    </style>
</head>
<body >
@include('frontend.layouts.partials.header')

@yield('content')

@include('frontend.layouts.partials.footer')


<!-- SCIPTS -->

<script src="{{asset('ui/frontend/js/jquery-3.1.1.min.js')}}"></script>

<script src="{{asset('ui/frontend/js/tether.min.js')}}"></script>

<script src="{{asset('ui/frontend/js/bootstrap.js')}}"></script>
<script src="http://cdn.bootcss.com/toastr.js/latest/js/toastr.min.js"></script>
{!! Toastr::message() !!}
<script>
    @if($errors->any())


        @foreach($errors->all() as $error)
            toastr.error('{{$error}}','Error',{
        closeButton: true,
        progressBar : true
    });
    @endforeach
    @endif
</script>


@stack('js')

</body>
</html>
