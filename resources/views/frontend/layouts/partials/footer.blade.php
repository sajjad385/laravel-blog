





<section id="subscribers">

<div class="container">
    <div class="row">

        <div class="col-md-6">
            <h3  class="text-center text-uppercase">Please Subscribe</h3>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class=" text-center input-area">
                <form action="{{route('subscriber.store')}}" method="post">
                    @csrf
                    <input class="email-input" type="email" name="email" placeholder="Enter your email">
                    <button class="submit-btn" type="submit"><i class="icon ion-ios-email-outline"></i></button>
                </form>
            </div>
        </div><!-- col-lg-4 col-md-6 -->
    </div>
</div>

</section>
<!--sub footer starts-->
<section id="sub_footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 about"
                 style="visibility: visible; animation-name: fadeInUp;" data-wow-delay=".3s">
                <h3 class="">About Us</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam facere incidunt laborum recusandae?
                </p>

            </div>
            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 contact"
                 style="visibility: visible; animation-name: fadeInUp;" data-wow-delay=".5s">
                <h3 class="">Contact Info</h3>
                <p>Contact No: <a href="tel:+880335843754">+880335843754</a></p>
                <p>Email : <a href="mailto:consult@gmail.com">blog@gmail.com</a></p>
                <p>Address: West Tejturi Bazar,Dhaka,Bangladesh</p>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 service"
                 style="visibility: visible; animation-name: fadeInUp;" data-wow-delay=".7s">
                <h3>Quick Links</h3>
                <p class="text-capitalize"> &#187; <a>PHP</a></p>
                <p class="text-capitalize"> &#187; <a> Laravel </a></p>
                <p class="text-capitalize"> &#187; <a>Python</a></p>
                <p class="text-capitalize"> &#187; <a>JavaScript</a></p>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 tags"
                 style="visibility: visible; animation-name: fadeInUp;" data-wow-delay=".7s">
                <h3 class="">Tags</h3>
                @foreach($tags as $tag)
                <a class="btn btn-primary" href="{{route('tag.posts',$tag->slug)}}">{{$tag->name}}</a>
                    @endforeach
            </div>
        </div>
    </div>
</section>
<!--sub footer end-->

<!--Foooter Section Starts-->
<footer>
    <div class="container">
        <p class="text-center" data-wow-delay=".2s" data-wow-duration="3s"> All Rights
            Reserved By <a href="#" target="blank">MSHS</a></p>
    </div>
</footer>