## Installation
- git clone `https://gitlab.com/sajjad385/laravel-blog.git`
- `composer update`
- `cp .env.example .env`
- `php artisan key:generate`
- `php artisan serve`
