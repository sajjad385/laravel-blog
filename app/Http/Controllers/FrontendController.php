<?php

namespace App\Http\Controllers;

use App\Category;
use App\Post;
use Illuminate\Http\Request;

class FrontendController extends Controller
{
    public function index()
    {

        $categories=Category::latest()->get();
        $posts= Post::latest()->approved()->published()->take(9)->get();
        return view('frontend.index',compact('categories','posts'));
    }
}
