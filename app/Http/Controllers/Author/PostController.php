<?php

namespace App\Http\Controllers\Author;

use App\Category;
use App\Notifications\NewAuthorPost;
use App\Post;
use App\Tag;
use App\User;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Auth::User()->posts()->latest()->get();
        return view('author.post.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $categories = Category::all();
        $tags = Tag::all();
        return view('author.post.create', compact('categories', 'tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'image' => 'required|mimes:jpg,png,jpeg',
            'categories' => 'required',
            'tags' => 'required',
            'body' => 'required',
        ]);


        $data = new Post();
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $name = date('y-m-d') . '_' . str_slug($request->title) . '.' .$image->getClientOriginalExtension($image);
            $destinationPath = public_path('/uploads/post');
            $imagePath = $destinationPath . "/" . $name;
            $image->move($destinationPath, $name);
            $data->image = $name;
        }
        $data->user_id = Auth::id();
        $data->title = $request->title;
        $slug = Str::slug($request->title);
        $data->image = $data['image'];
        $data->slug = $slug;
        $data->body = $request->body;
        if (isset($request->status)) {
            $data->status = true;
        } else {
            $data->status = false;
        }
        $data->is_approved = false;

        $data->save();
        $data->categories()->attach($request->categories);
        $data->tags()->attach($request->tags);
        
        $users= User::where('role_id','1')->get();
        Notification::send($users, new NewAuthorPost($data));
        Toastr::success('Post Successfully Inserted :)', 'Success');
        return redirect()
            ->route('author.post.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {

        if($post->user_id !=Auth::id())
        {
            Toastr::error('You are not authorized  to access this post', 'Error');
            return redirect()->back();
        }

        else
        return view('author.post.show',compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */

    public function edit(Post $post)
    {
        if($post->user_id !=Auth::id())
        {
            Toastr::error('You are not authorized  to access this post', 'Error');
            return redirect()->back();
        }
        else
        $categories = Category::all();
        $tags = Tag::all();
        return view('author.post.edit', compact('post','categories', 'tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        if($post->user_id !=Auth::id())
        {
            Toastr::error('You are not authorized  to access this post', 'Error');
            return redirect()->back();
        }

        else



        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'categories' => 'required',
            'tags' => 'required',
            'body' => 'required',
        ]);


        $data = new Post();
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $name = date('y-m-d') . '_' . str_slug($request->title) . '.' .$image->getClientOriginalExtension($image);
            $destinationPath = public_path('/uploads/post');
            $imagePath = $destinationPath . "/" .$name;
            $image->move($destinationPath, $name);
            $data->image = $name;
        }
        else {
            $data->image = $post->image;
        }
        $data->user_id = Auth::id();
        $data->title = $request->title;
        $slug = Str::slug($request->title);
        $data->image = $data['image'];
        $data->slug = $slug;
        $data->body = $request->body;
        if (isset($request->status)) {
            $data->status = true;
        } else {
            $data->status = false;
        }
        $data->is_approved = false;
        $data->save();
        $data->categories()->sync($request->categories);
        $data->tags()->sync($request->tags);


        Toastr::success('Post Successfully Updated :)', 'Success');
        return redirect()
            ->route('author.post.index');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        if($post->user_id !=Auth::id())
        {
            Toastr::error('You are not authorized  to access this post', 'Error');
            return redirect()->back();
        }

        else

        if($post->image)
        {
            $image_path = public_path().'/uploads/post/'.$post->image;
            unlink($image_path);

        }
        else{

        }
        $post->categories()->detach();
        $post->tags()->detach();
        $post->delete();
        Toastr::success('Category Successfully Deleted :)', 'Success');
        return redirect()
            ->route('author.post.index');

        }
}
