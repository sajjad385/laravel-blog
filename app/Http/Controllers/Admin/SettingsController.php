<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Str;

class SettingsController extends Controller
{

    const UPLOAD_DIR = '/uploads/profile/';

    public function index()
    {
        return view('admin.settings');
    }

    public function updateProfile(Request $request)
    {
        $this->validate($request, [

            'name' => 'required',
            'email' => 'required|email',
        ]);



        $data = Str::slug($request->name);
        $data = User::findOrFail(Auth::id());

        if ($request->hasFile('image')) {
            $data['image'] = $this->uploadImage($request->image, $request->name);
        }

        $data->name=$request->name;
        $data->email=$request->email;
        $data->image=$data['image'];
        $data->about=$request->about;
        $data->update();
        Toastr::success('Profile Successfully Updated :)', 'Success');
        return redirect()->back();


    }

    public function updatePassword(Request $request){
        $this->validate($request, [

            'old_password' => 'required',
            'password' => 'required|confirmed',
        ]);

        $hashedPassword =Auth::user()->password;
        if (Hash::check($request->old_password,$hashedPassword)){

            if (!Hash::check($request->password,$hashedPassword)){

                $user =User::findOrFail(Auth::id());
                $user->password= Hash::make($request->password);
                $user->save();
                Toastr::success('Password Successfully Updated :)', 'Success');
                Auth::logout();
                return redirect()->back();
            }
            else
            {
                Toastr::error('New Password Cannot be the same as old password.', 'Error');
                return redirect()->back();


            }

        } else {
            Toastr::error('Current Password not match. ', 'Error');
            return redirect()->back();
        }



    }


    private function uploadImage($file, $name)
    {

        $extension = $file->getClientOriginalExtension($file);
        $fileName = date('y-m-d') . '_' . $name . '.' . $extension;
        Image::make($file)->resize(600, 600)->save(public_path() . self::UPLOAD_DIR . $fileName);


        return $fileName;
    }

}
