<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Str;

class CategoryController extends Controller
{


    const UPLOAD_DIR = '/uploads/category/';
    const UPLOAD_DIR1 = '/uploads/category/slider/';

    public function index()
    {
        $categories= Category::latest()->get();
        return view('admin.category.index',compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:categories',
            'image' => 'required|mimes:jpg,png,jpeg',
        ]);


        if ($validator->fails()) {
            return redirect()->route('admin.category.create')
                ->withErrors($validator)
                ->withInput();
        }

        try {
            $data=new Category();
            $data->name=$request->name;
            $data->slug= Str::slug($request->name);
            if($request->hasFile('image')){
                $data['image'] = $this->uploadImage($request->image, $request->name);
            }


            $data->save();
            Toastr::success('Category Successfully Inserted :)', 'Success');
            return redirect()
                ->route('admin.category.index');
        }

        catch (QueryException $e)

        {
            return redirect()
                ->route('banners.create')
                ->withInput()
                ->withErrors($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category=Category::find($id);
        return view('admin.category.edit',compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'image' => 'mimes:jpg,png,jpeg',
        ]);


        if ($validator->fails()) {
            return redirect()->route('admin.category.edit')
                ->withErrors($validator)
                ->withInput();
        }

        try {

            $data=Category::find($id);
            $data->name=$request->name;
            $data->slug= Str::slug($request->name);
            if($request->hasFile('image')){
                $data['image'] = $this->uploadImage($request->image, $request->name);
            }


            $data->update();
            Toastr::success('Category Successfully Updated :)', 'Success');
            return redirect()
                ->route('admin.category.index');
        }

        catch (QueryException $e)

        {
            return redirect()
                ->route('banners.create')
                ->withInput()
                ->withErrors($e->getMessage());
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        $category=Category::findOrFail($id);


        $image_path = public_path().'/uploads/category/'.$category->image;
        $image_path = public_path().'/uploads/category/slider/'.$category->image;
        unlink($image_path);
        $category->delete();
        Toastr::success('Category Successfully Deleted :)', 'Success');
        return redirect()
            ->route('admin.category.index');
    }



    private function uploadImage($file, $name)
    {

        $extension = $file->getClientOriginalExtension($file);
        $fileName =date('y-m-d').'_'.$name.'.'.$extension;
        Image::make($file)->resize(1600,479)->save(public_path().self::UPLOAD_DIR.$fileName);

        $fileName = date('y-m-d').'_'.$name.'.'.$extension;
        Image::make($file)->resize(500,333)->save(public_path().self::UPLOAD_DIR1.$fileName);
        return $fileName;
    }



}





