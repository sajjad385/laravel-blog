<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Notifications\AuthorPostApproved;
use App\Notifications\NewPostNotify;
use App\Post;
use App\Subscriber;
use App\Tag;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    const UPLOAD_DIR = '/uploads/post/';

    public function index()
    {
        $posts = Post::latest()->get();
        return view('admin.post.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        $tags = Tag::all();
        return view('admin.post.create', compact('categories', 'tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'image' => 'required|mimes:jpg,png,jpeg',
            'categories' => 'required',
            'tags' => 'required',
            'body' => 'required',
        ]);


        $data = new Post();
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $name = date('y-m-d') . '_' . str_slug($request->title) . '.' . $image->getClientOriginalExtension($image);
            $destinationPath = public_path('/uploads/post');
            $imagePath = $destinationPath . "/" . $name;
            $image->move($destinationPath, $name);
            $data->image = $name;
        }
        $data->user_id = Auth::id();
        $data->title = $request->title;
        $slug = Str::slug($request->title);
        $data->image = $data['image'];
        $data->slug = $slug;
        $data->body = $request->body;
        if (isset($request->status)) {
            $data->status = true;
        } else {
            $data->status = false;
        }
        $data->is_approved = true;
        $data->save();
        $data->categories()->attach($request->categories);
        $data->tags()->attach($request->tags);
        $subscribers=Subscriber::all();
        foreach ($subscribers as $subscriber)
        {
            Notification::route('mail',$subscriber->email)
                ->notify(new NewPostNotify($data));
        }

        Toastr::success('Post Successfully Inserted :)', 'Success');
        return redirect()
            ->route('admin.post.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        return view('admin.post.show', compact('post'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        $categories = Category::all();
        $tags = Tag::all();
        return view('admin.post.edit', compact('post', 'categories', 'tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Post $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'categories' => 'required',
            'tags' => 'required',
            'body' => 'required',
        ]);


        $data = new Post();
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $name = date('y-m-d') . '_' . str_slug($request->title) . '.' . $image->getClientOriginalExtension($image);
            $destinationPath = public_path('/uploads/post');
            $imagePath = $destinationPath . "/" . $name;
            $image->move($destinationPath, $name);
            $data->image = $name;
        } else {
            $data->image = $post->image;
        }
        $data->user_id = Auth::id();
        $data->title = $request->title;
        $slug = Str::slug($request->title);
        $data->image = $data['image'];
        $data->slug = $slug;
        $data->body = $request->body;
        if (isset($request->status)) {
            $data->status = true;
        } else {
            $data->status = false;
        }
        $data->is_approved = true;
        $data->save();
        $data->categories()->sync($request->categories);
        $data->tags()->sync($request->tags);


        Toastr::success('Post Successfully Updated :)', 'Success');
        return redirect()
            ->route('admin.post.index');
    }


    public function pending()
    {
        $posts = Post::where('is_approved', false)->get();
        return view('admin.post.pending', compact('posts'));
    }

    public function approval($id)
    {
        $post= Post::findOrFail($id);
        if($post->is_approved == false){

            $post->is_approved = true;
            $post->save();

            $post->user->notify(new AuthorPostApproved($post));
            $subscribers=Subscriber::all();
        foreach ($subscribers as $subscriber)
        {
            Notification::route('mail',$subscriber->email)
                ->notify(new NewPostNotify($post));
        }


            Toastr::success('Post Successfully Approved :)', 'Success');


        }else{
            Toastr::success('This post is already approved :)', 'Info');

        }
        return redirect()->back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        if ($post->image) {
            $image_path = public_path() . '/uploads/post/' . $post->image;
            unlink($image_path);

        } else {

        }
        $post->categories()->detach();
        $post->tags()->detach();
        $post->delete();
        Toastr::success('Category Successfully Deleted :)', 'Success');
        return redirect()
            ->route('admin.post.index');
    }


}
